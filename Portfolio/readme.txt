Zusammenfassung der behandelten Konzepte in den einzelnen Kapiteln:

Folgende Chapters haben einen direkten Link auf der Portfoliowebsite:
Chapter 1,2,3,4,5 
Chapter 8, 9 im CV link
Chapter 10, 11 und 13 

Chapter 6,7 und 12:
Auf deiner Portfolioseite habe ich versucht, u.a. die Konzepte aus den Kapiteln 6, 7 und 12 anzuwenden, 
um ansprechende Designs mit Sass, Media Queries und Animationen zu erstellen.

Chapter Exercise 1 bis 5:
- Listen (List Elements): Verwendung von geordneten (ordered) und ungeordneten (unordered) Listen.
- Blöcke (Blocks): Grundlegende Elemente, die im Flusslayout der Webseite verwendet werden.
- Klassen (Classes) und IDs: Verwendung von Klassen und IDs zur Identifizierung und Formatierung von HTML-Elementen.
- Semantische Elemente: Verwendung semantischer HTML-Elemente wie `<header>`, `<nav>`, `<section>`, `<article>`, `<footer>` usw.
- Grafiken (Graphics): Einbinden von Bildern in die Webseite.
- Farben (Colors): Verwendung von Farben für Hintergrund, Text und andere Elemente.
- Textformatierung: Anpassung von Schriftgröße, Schriftart, Ausrichtung usw.
- Schriftarten (Fonts): Einbindung und Anwendung verschiedener Schriftarten.
- Layout: Grundlegende Strukturierung der Webseite mit Hilfe von HTML-Elementen und CSS-Regeln.

Chapter Exercise 8:
- Bootstrap: Einführung in das Bootstrap-Framework zur Erstellung von responsiven und gut gestalteten Webseiten.

Chapter 9:
- Rechtliche Begriffe: Einführung in rechtliche Aspekte im Zusammenhang mit Webseiten und deren Inhalten.

Chapter 10, 11 und 13:
- Funktionen und Berechnungen: Verwendung von JavaScript-Funktionen und Durchführung von Berechnungen.
- Objekte: Verständnis von JavaScript-Objekten und deren Verwendung in der Webentwicklung.

Chapter 6, 7 und 12:
- Sass-Konzepte: Verwendung von Sass zur effizienten und modularen Entwicklung von CSS.
- Media Queries: Anpassung des Layouts und der Stile basierend auf verschiedenen Bildschirmgrößen und Geräten.
- Animationen: Erzeugung von CSS-Animationen und Übergängen für eine interaktive und ansprechende Benutzererfahrung.